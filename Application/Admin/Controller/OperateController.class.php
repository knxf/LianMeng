<?php

namespace Admin\Controller;
use User\Api\UserApi as UserApi;
use User\Api\Validate;

class OperateController extends AdminController {

    /**
     * 需求列表
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function index(){
        $nickname = I('nickname', '', 'trim');
        $map = [];
        if($nickname) $map['name'] = ['LIKE', "%{$nickname}%"];
        $res = $this->lists('Project', $map);

        $this->assign('_list', $res);
        $this->display();
    }

    /*
     * 项目详情
     */
    public function proinfo($id){
        $id || $id = I('id', '', 'intval');
        if(!$id){
            $this->error("非法操作");
        }

        $map = ['id'=>$id];
        $info = M('Project')->where($map)->find();

        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 审核通过
     */
    public function succPro(){
        //$id = array_unique((array)I('ids',0));
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [ 'status' => ['IN', [0, 2]] ];
        $map['id'] = ['IN', $ids];
        $model  = M('Project');
        $flag   = $model->where($map)->save(['status'=>1]);
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /**
     * 审核失败
     */
    public function denyPro(){
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [ 'status' => 2 ];
        $map['id'] = ['IN', $ids];
        $model  = M('Project');
        $flag   = $model->where($map)->save(['status'=>0]);
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /**
     * 报价列表
     */
    public function quotes(){
        $map = [];

        $nickname = I('nickname', '', 'trim');
        if($nickname){
            $where = ['name'=>['LIKE', "%{$nickname}%"]];
            $ids = M('Project')->where($where)->getField('id',true);

            if($ids) $map['pid'] = ['IN', $ids];
        }
        $res = $this->lists('ProjectOrder', $map);

        $this->assign('_list', $res);
        $this->assign('nickname', $nickname);
        $this->display();
    }

    /**
     * 删除竞标方案
     */
    public function delQuotes(){
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [];
        $map['id'] = ['IN', $ids];
        $model  = M('ProjectOrder');
        $flag   = $model->where($map)->delete();
        $sql = $model->getLastSql();
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /**
     * 服务商列表
     */
    public function tech(){
        $map = [];

        $nickname = I('nickname', '', 'trim');
        if($nickname){
            $map['name|mobile'] = ['LIKE', "%{$nickname}%"];
        }
        $res = $this->lists('provider', $map);

        $this->assign('_list', $res);
        $this->display();
    }

    /*
     * 编辑.新增人才
     */
    public function savetech(){
        $id = I('id', '', 'intval');
        $model = M('provider');
        if(IS_POST){
            $name       = I('name', '', 'trim');
            $mobile     = I('mobile', '', 'trim');
            $address    = I('address', '', 'trim');
            $divide     = I('divide', '', 'trim');
            $exprience  = I('exprience', '', 'trim');
            $school     = I('school', '', 'trim');
            $persondesc = I('persondesc', '', 'trim');

            $data = [];
            $data['name']       = $name;
            $data['mobile']     = $mobile;
            $data['address']    = $address;
            $data['divide']     = $divide;
            $data['exprience']  = $exprience;
            $data['school']     = $school;
            $data['persondesc'] = $persondesc;

            $nowTime = time();
            $data['update_time']= $nowTime;

            if( !Validate::mobi($mobile) ){
                $this->error('手机号格式不正确');
            }

            $info = $model->where([ 'mobile' => $mobile ])->find();
            if($info){
                $flag = $model->where([ 'id' => $info['id'] ])->save($data);
            }else{
                $data['create_time'] = $nowTime;
                $flag = $model->add($data);
            }

            if(false === $flag){
                $this->error('操作失败');
            }else{
                $this->success('操作成功', U('tech'));
            }
        }elseif ($id){
            $info = $model->where([ 'id' => $id ])->find();
            $this->assign('info', $info);
        }

        $this->display();
    }

    /**
     * 通过人才入驻
     */
    public function succTech(){
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [ 'status' => ['IN', [0, 2]] ];
        $map['id'] = ['IN', $ids];
        $model  = M('Provider');
        $flag   = $model->where($map)->save(['status'=>1]);
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /**
     * 拒绝人才入驻申请
     */
    public function denyTech(){
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [ 'status' => ['IN', [0, 2]] ];
        $map['id'] = ['IN', $ids];
        $model  = M('Provider');
        $flag   = $model->where($map)->save(['status'=>0]);
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /**
     * 删除不合格人才
     */
    public function delTech(){
        $ids = I('id', '', 'trim');
        if( !$ids ){
            $this->error("请选择操作对象");
        }

        if( !is_array($ids) ){
            $ids = explode(',', $ids);
        }

        $map = [];
        $map['id'] = ['IN', $ids];
        $model  = M('Provider');
        $flag   = $model->where($map)->delete();
        if($flag === false){
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }
}
