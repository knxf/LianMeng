<?php
/**
 * 工作类型
 * User: wayne
 * Date: 2018/3/17 13:03
 */
namespace Wx\Controller;

class WorkerController extends BaseController {

    /**
     * 项目列表
     */
    public function lists(){
        $typeArr  = M('ProjectType')->select();
        $indArr   = M('ProjectIndustry')->select();
        $this->assign('_types', $typeArr);
        $this->assign('_industry', $indArr);

        $map = ['status'=>1];
        $type = I('type', '', 'intval');
        if( $type ){
            $map['type'] = $type;
        }
        $industry = I('industry', '', 'intval');
        if( $industry ){
            $map['industry'] = $industry;
        }
        $this->assign('type', $industry);
        $this->assign('industry', $industry);

        $proModel = M('Project');
        $total = $proModel->where($map)->count();
        //
        $REQUEST['r'] = 5;
        if( isset($REQUEST['r']) ){
            $listRows = (int)$REQUEST['r'];
        }else{
            $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
        }

        $page = new \Think\Page($total, $listRows, $REQUEST);
        if($total>$listRows){
            $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p =$page->show();
        $this->assign('_page',  $p? $p: '');
        $this->assign('_total', $total);

        //
        $res = $proModel->where($map)->order('update_time DESC')->limit($page->firstRow, $page->listRows)->select();
        if($res){
            $nowTime = time();
            foreach ($res as $k=>$v){
                $diedayTime = (isset($v['dieday']) && $v['dieday']) ? $v['dieday'] * 86400 : 604800;
                $endTime = intval($v['create_time']) + $diedayTime;//报价截止时间为一周
                $v['bidtime'] = ( $endTime > $nowTime ) ? ( $endTime - $nowTime ) : 0;

                $res[$k] = $v;
            }
        }
        $this->assign('_lists', $res);

        $this->display();
    }

    /**
     * 项目详情
     */
    public function detail(){
        $id = I('id', '', 'intval');
        if(!$id) $this->error('非法请求');

        //项目详情
        $info = M('Project')->where(['id'=>$id])->find();
        if($info){
            $nowTime = time();
            $diedayTime = (isset($info['dieday']) && $info['dieday']) ? $info['dieday'] * 86400 : 604800;
            $endTime = intval($info['create_time']) + $diedayTime;//报价截止时间为一周
            $info['bidtime'] = ( $endTime > $nowTime ) ? ( $endTime - $nowTime ) : 0;
        }
        $this->assign('info', $info);

        //是否已投标
        $order = M('ProviderOrder')->where(['id'=>$id, 'suid'=>$this->_uid])->find();
        $this->assign('hasbid', ($order) ? 1 : 0);
        $this->display();
    }

    /**
     * 提交报价
     */
    public function quotes(){
        $pid = I('pid', '', 'intval');
        if(!$pid){
            $this->error('请求异常');
        }

        if(IS_POST){
            $model  = M('ProjectOrder');
            $info   = $model->where(['pid'=>$pid, 'suid' => $this->_uid])->find();
            if($info){
                $this->error('你已经投标请耐心等待！');
            }

            $data = ['pid'=>$pid, 'suid' => $this->_uid];
            $data['duration']   = I('duration', '', 'trim');
            $data['price']      = I('price', '', 'trim');
            $data['linkname']   = I('username', '', 'trim');
            $data['linknum']    = I('mobile', '', 'trim');
            $data['difficulty'] = I('difficulty', '', 'trim');
            $data['solution']   = I('solution', '', 'trim');
            $data['advantage']  = I('advantage', '', 'trim');
            $data['create_time'] = time();

            $id = $model->add($data);
            if($id){
                //项目竞标人数+1
                M('Project')->where(['id'=>$pid])->setInc('bidnum');
                $this->success('报价成功');
            } else {
                $this->error('报价失败');
            }
        }

        $this->assign('pid', $pid);
        $this->display();
    }

    /**
     * 发布需求
     */
    public function publish(){
        if(IS_POST){
            $now = time();
            $data = ['create_time'=>$now, 'update_time'=>$now, 'duid'=>$this->_uid];
            $data['name']       = I('name', '', 'trim');
            $data['desc']       = I('desc', '', 'trim');

            $data['linkname']   = I('linkname', '', 'trim');
            $data['linknum']    = I('linknum', '', 'trim');

            $data['budget']     = I('budget', '', 'intval');
            $data['duration']   = I('duration', '', 'intval');
            $data['dieday']     = I('dieday', '', 'intval');
            $data['type']       = I('type', '', 'intval');
            $data['referencesc'] = I('reference', '', 'trim');

            $id = M('Project')->add($data);
            if($id){
                $this->success('发布成功', U('lists'));
            } else {
                $this->error('发布失败', U('lists'));
            }
        }

        $this->display();
    }

    /*
     * 标注项目完成
     */
    public function done(){
        $pid = I('pid', '', 'intval');
        if(!$pid){
            $this->error('非法请求');
        }

        $model = M('Project');
        $info = $model->where(['id'=>$pid])->find();
        if(!$info){
            $this->error('数据不存在');
        }

        if($info['duid'] != $this->_uid){
            $this->error('非法操作');
        }

        $flag = $model->where(['id'=>$pid])->save(['bidstate'=>1]);
        if(false !== $flag){
            $this->success('操作完成', U('User/publist'));
        }else{
            $this->error('操作失败');
        }

    }
}