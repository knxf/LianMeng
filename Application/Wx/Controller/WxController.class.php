<?php
// +----------------------------------------------------------------------
// | 微信公共控制器
// +----------------------------------------------------------------------
// | Author: wyqiang <2925528279@qq.com>
// +----------------------------------------------------------------------
namespace Wx\Controller;
use Think\Controller;
class WxController extends Controller{
    protected $openid   = '';
    protected $appinfo  = [];
    protected $userinfo = [];
    protected $cookieopenid = '';
    protected $cookieaccess = '';
    protected $apiUri   = 'https://api.weixin.qq.com/sns/userinfo?';

    /* 空操作，用于输出404页面 */
    public function _empty(){
        $this->error('找不到URL');exit;
    }

    protected function _initialize(){
        if(!isWeixinBrowser()){
            $this->error('请使用微信浏览器');exit;
        }

        //公众号信息
        $this->appinfo = array(
            'app_id' => C('APPID'),
            'app_secret' => C('APPSECRET'),
            'type' => 2
        );

        $this->cookieopenid = 'chang_openid_' . C('APP_ID');
        $this->cookieaccess = 'chang_access_' . C('APP_ID');
        $this->openid = $this->_get_openid();
    }

    /**
     * 获取openid
     * @return mixed
     */
    private function _get_openid(){
        $openid = I('openid');
        $accessToken = I('access_token');
        if(!$openid){
            $openid         = cookie($this->cookieopenid);
            $accessToken    = cookie($this->cookieaccess);

            if(!$openid){
                $openid = getOpenid($this->appinfo);

                if(-1 == $openid) return false;
            }
        }else{
            cookie($this->cookieopenid, $openid);
            cookie($this->cookieaccess, $accessToken, 3600);//缓存
        }

        // 若openid为空，且不是第一步获取code
        if(!$openid && !I('code')){
            exit('获取微信授权失败');
        }

        // 判断用户信息
        $userModel = M('user');
        $info = $userModel->where(['openid'=>$openid])->find();
        $monthTime = time() - 2592000;//大于一个月
        if($accessToken && (!$info || $info['create_time'] <  $monthTime)){
            $params = ['access_token'=>$accessToken, 'openid'=>$openid];
            $userUri = $this->apiUri . http_build_query($params);
            $wxArr = curl_get($userUri);
            if(!$wxArr){
                exit('网络异常');
            }

            $nowTime = time();
            if(!$info){
                $info['nickname']   = $wxArr['nickname'];
                $info['unionid']    = $wxArr['unionid'];
                $info['openid']     = $wxArr['openid'];
                $info['sex']        = $wxArr['sex'];
                $info['language']   = $wxArr['language'];
                $info['city']       = $wxArr['city'];
                $info['province']   = $wxArr['province'];
                $info['country']    = $wxArr['country'];
                $info['headimgurl'] = $wxArr['headimgurl'];
                $info['privilege']  = $wxArr['privilege'];
                $info['create_time']    = $nowTime;
                $info['modify_time']    = $nowTime;
                $id = $userModel->add($info);
            } else {
                $info['nickname']       = $wxArr['nickname'];
                $info['modify_time']    = $nowTime;
                $flag = $userModel->where(['id'=>$info['id']])->save($info);
            }
        }
        $this->userinfo = $info;
        return ($openid) ? $openid : false;
    }

    /**
     * 判断绑定
     * 新绑定逻辑.绑定为默认地址
     */
    private function judge_bind(){
        $AddressModel = D('Address');
        $isBind = $AddressModel->judge_mobile_bind_wx($this->openid);
        if(!$isBind){
            redirect(U('Address/edit'));
        }
    }
}
