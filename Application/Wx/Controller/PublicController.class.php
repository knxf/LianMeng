<?php
/**
 * 公共类
 * User: wayne
 * Date: 2018/5/1 13:27
 */
namespace Wx\Controller;
use Think\Controller;
use Think\Verify;
use User\Api\UserApi;
use User\Api\Validate;

class PublicController extends Controller{
    /**
     * 入驻(手机号绑定openid)
     */
    public function register() {
        if (IS_POST) {
            $checkcode  = I('check_code');
            $verify     = new Verify();
            $verify_check = $verify->check($checkcode, 'userInfo');
            if (!$verify_check){
                $this->error('验证码错误', U('Public/register'));
            }

            $username   = I('true_name', '', 'trim');
            $mobile     = I('mobile', '', 'trim');
            $password   = I('password', '', 'trim');
            $repassword = I('repassword', '', 'trim');
            $validate = new Validate();
            if( !$validate::required($username) ) {
                $this->error('昵称不能为空', U('Public/register'));
            }
            if( !$validate::mobi($mobile) ) {
                $this->error('手机号错误', U('Public/register'));
            }
            if (!$username || !$mobile || !$password || !$repassword) $this->error('所有项都不能为空', U('Public/register'));
            if ($password != $repassword) $this->error('两次输入的密码不一致', U('Public/register'));

            $userApi = new UserApi();
            $email = $mobile.'@lmeng.com';
            $uid     = $userApi->register($username, $password, $email, $mobile);
            if ($uid > 0) {
                $this->success('注册成功', U('Public/login'));
            } else {
                $this->error($this->showRegError($uid), U('Public/register') );
            }
            return;
        }

        $this->display();
    }

    /**
     * 用户登录方法
     */
    public function login() {
        if (IS_POST) {
            $verify       = new Verify();
            $checkcode    = I('check_code');
            $verify_check = $verify->check($checkcode, 'userInfo');
            if (!$verify_check){
                $this->error('验证码错误',U('Public/login'));
            }

            $mobile = I('mobile');
            $password = I('password');
            if (!$mobile || !$password) {
                $this->ajaxReturn(['code'=>1,'msg'=>'缺少重要参数'],'json') ;
            }
            $user = new UserApi();
            $uid = $user->login($mobile, $password, 3);

            $role = I('role');
            $redirectUrl = ( $role == 'jia' ) ? U('User/publist') : U('User/center');
            if( 0 < $uid ) { // UC登录成功
                $Member = D('Member');
                if ($Member->login($uid)) {
                    user_role($role);
                    $this->success('登录成功！', $redirectUrl);
                } else {
                    $this->error($Member->getError());
                }
            } else {
                switch($uid) {
                    case -1: $error = '用户不存在或被禁用！'; break; //系统级别禁用
                    case -2: $error = '密码错误！'; break;
                    default: $error = '未知错误！'; break; // 0-接口参数错误（调试阶段使用）
                }
                $this->error($error, U('Public/login'));
            }

            return;
        }

        $this->display('login');
    }

    /**
     * 用户登出
     */
    public function logout()
    {
        session_destroy();
        $this->success('正在退出', U('Public/login'));
    }

    /**
     * 产生不带背景的验证码
     */
    public function getVerifyCode()
    {
        $verify = new Verify();
        $verify->entry('userInfo');
    }

    /**
     * 获取用户注册错误信息
     * @param  integer $code 错误编码
     * @return string        错误信息
     */
    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '用户名长度必须在16个字符以内！'; break;
            case -2:  $error = '用户名被禁止注册！'; break;
            case -3:  $error = '用户名被占用！'; break;
            case -4:  $error = '密码长度必须在6-30个字符之间！'; break;
            case -5:  $error = '邮箱格式不正确！'; break;
            case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -7:  $error = '邮箱被禁止注册！'; break;
            case -8:  $error = '邮箱被占用！'; break;
            case -9:  $error = '手机格式不正确！'; break;
            case -10: $error = '手机被禁止注册！'; break;
            case -11: $error = '手机号被占用！'; break;
            default:  $error = '未知错误';
        }
        return $error;
    }
}