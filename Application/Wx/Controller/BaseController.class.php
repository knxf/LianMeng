<?php
/**
 * 基控制器
 * User: wyqiang
 * Date: 2018/5/1 13:17
 */
namespace Wx\Controller;
use Think\Controller;

class BaseController extends Controller{
    protected $_uid;

    protected $registerError = array(
        -1  => '用户名长度不合法',
        -2  => '用户名禁止注册',
        -3  => '用户名被占用',
        -4  => '密码长度不合法',
        -5  => '邮箱格式不正确',
        -6  => '邮箱长度不合法',
        -7  => '邮箱禁止注册',
        -8  => '邮箱被占用',
        -9  => '手机格式不正确',
        -10 => '手机禁止注册',
        -11 => '手机号被占用',
    );
    protected $loginErrorInfo  = array(
        -1 => '用户不存在或被禁用',
        -2 => '密码错误',
    );

    /*
    * 空操作，用于输出404页面
    */
    public function _empty(){
        $this->redirect('Index/index');
    }

    /*
     * 读取站点配置
     */
    protected function _initialize(){
        define('UID', is_login());
        if( !UID ){// 还没登录 跳转到登录页面
            $this->error('您还没有登录，请先登录！', U('Public/login'));
        }

        //添加配置
        $config = api('Config/lists');
        C($config);
        if(!C('WEB_SITE_CLOSE')){
            $this->error('站点已经关闭，请稍后访问~');
        }

        //判断用户角色
        $user_role = user_role();
        if($user_role){
            $this->assign('user_role', $user_role);
        }

        $this->_uid = UID;
        $this->assign('user_id', $this->_uid);
    }

    /*
     * 用户登录检测
     */
    protected function login(){
        /* 用户登录检测 */
        is_login() || $this->error('您还没有登录，请先登录！', U('User/login'));
    }
}