<?php
/**
 * 工作类型
 * User: wayne
 * Date: 2018/3/17 13:03
 */
namespace Wx\Controller;

class TechController extends BaseController {
    /**
     * 项目列表
     */
    public function lists(){
        $title = I('title', '', '');

        $map = ['status'=>1];
        if($title) $map['name'] = ['LIKE', "%{$title}%"];

        //分页
        $proModel = M('provider');
        $total = $proModel->where($map)->count();
        $REQUEST['r'] = 5;
        if( isset($REQUEST['r']) ){
            $listRows = (int)$REQUEST['r'];
        }else{
            $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
        }

        $page = new \Think\Page($total, $listRows, $REQUEST);
        if($total>$listRows){
            $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p =$page->show();
        $this->assign('_page',  $p? $p: '');
        $this->assign('_total', $total);

        //列表数据
        $res = $proModel->where($map)->order('update_time DESC')->limit($page->firstRow, $page->listRows)->select();
        $this->assign('_lists', $res);
        $this->assign('title', $title);

        $this->display();
    }

    /**
     * 详情
     */
    public function detail(){
        $id = I('id', '', 'intval');
        if(!$id) $this->error('非法请求');

        $info = M('Provider')->where(['id'=>$id])->find();

        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 提交需求表单
     */
    public function offerform(){
        $suid = I('suid', '', 'intval');
        if(IS_POST){

        }

        $this->display();
    }
}