<?php
/**
 * User: wyqiang
 * Date: 16-10-12 下午6:03
 */
namespace Wx\Controller;

class IndexController extends BaseController {
    /**
     * 机票列表
     */
    public function index(){
        $map = [];
        $res = M('Ticket')->where($map)->order('id DESC')->limit(50)->select();

        $this->assign('_lists', $res);
        $this->display();
    }

    /**
     * 机票详情
     */
    public function info(){
        $id = I('id', '', 'intval');
        if(!$id) $this->error('数据不存在');

        $info = M('Ticket')->where(['id'=>$id])->find();
        $this->assign('info', $info);
        $this->display();
    }

    /**
     * 收藏
     */
    public function fav(){
        $id = I('id', '', 'intval');
        $uid = $this->userinfo['id'];
        if(!$id) $this->error('数据不存在');
        if(!$uid) $this->error('用户数据异常');

        $favModel = M('TicketFav');

        //非重复操作
        $fav = $favModel->where(['ticket_id'=>$id, 'wxid'=>$uid])->find();
        if($fav){
            $this->success('已收藏');
        }

        //添加到收藏库
        $data = [
            'ticket_id'     => $id,
            'wxid'          => $uid,
            'create_time'   => time(),
            'update_time'   => time(),
        ];
        $flag =$favModel->add($data);
        if($flag){
            $this->success('收藏成功');
        }else{
            $this->error('操作失败');
        }
    }

    public function buy(){}

    /**
     * 关于我们
     */
    public function about(){
        $this->display();
    }
}