<?php
/**
 * 用户
 * User: wayne
 * Date: 2018/3/17 12:23
 */
namespace Wx\Controller;

class UserController extends BaseController {

    /**
     * 服务方.个人中心
     */
    public function center(){
        $model = M('provider');
        if(IS_POST){
            $nowTime = time();
            $data = [ 'uid' => $this->_uid];
            $data['name']       = I('name', '', 'trim');
            $data['mobile']     = I('mobile', '', 'trim');
            $data['exprience']  = I('exprience', '', 'trim');
            $data['school']     = I('school', '', 'trim');
            $data['working']    = I('working', '', 'trim');
            $data['divide']     = I('divide', '', 'trim');
            $data['address']    = I('address', '', 'trim');
            $data['skill']      = I('skill', '', 'trim');
            $data['persondesc'] = I('persondesc', '', 'trim');
            $data['workdesc']   = I('workdesc', '', 'trim');
            $data['edudesc']    = I('edudesc', '', 'trim');
            $data['update_time'] = $nowTime;

            $info = $model->where(['uid'=>$this->_uid])->find();
            if($info){
                if(0 == $info['status']){
                    $this->error('您的资料已进入黑名单，请联系客服');
                }

                $flag = $model->where(['uid'=>$this->_uid])->save($data);
            }else{
                $data['create_time'] = time();
                $id = $model->add($data);
                $flag = $id;
            }

            // 返回结果
            if($flag !== false){
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }

        }else{
            $info = $model->where(['uid'=>$this->_uid])->find();
            if ($info) {
                redirect('/Wx/User/myworker');
            }
            $this->assign('info', $info);
        }

        $this->display();
    }

    /**
     * 服务方.个人案例
     */
    public function techcase(){
        if(IS_POST){

        }

        $this->display();
    }

    /**
     * 服务方.参与过的项目
     */
    public function myworker(){
        $map = ['suid'=>$this->_uid];
        $status = I('status', '', 'intval');
        if($status){
            $map['status'] = $status;
        }
        $res = M('ProjectOrder')->where($map)->order('id DESC')->select();
        if($res){
            //填充项目名称
            $ids = array_column($res, 'pid');
            $proArr = M('Project')->where(['id'=>['IN', $ids]])->getField('id,name');
            //填充状态
            $statArr = [0=>'未中标',1=>'已结束',2=>'已中标',3=>'竞标中',4=>'报价中'];

            foreach ($res as $k=>$v){
                $name = '无名称';
                if(array_key_exists($v['pid'], $proArr)){
                    $name = $proArr[$v['pid']];
                }
                $v['project_name'] = $name;
                $v['status_text'] = $statArr[$v['status']];

                $res[$k] = $v;
            }
        }

        $this->assign('_lists', $res);
        $this->assign('status', $status);
        $this->display();
    }


    /**
     * 我发布项目的竞标者列表
     */
    public function mywooer(){
        $projectId = I('id', '', 'intval');
        if(!$projectId){
            $this->error('非法请求');
        }

        $res = M('ProviderOrder')->where(['id'=>$projectId])->find();
        $this->assign('_lists', $res);

        $this->display();
    }

    /**
     * 需求方.个人中心.我发布的项目
     */
    public function publist(){
        $map = [ 'duid' => $this->_uid ];

        $proModel = M('Project');
        $total = $proModel->where($map)->count();
        //
        $REQUEST['r'] = 5;
        if( isset($REQUEST['r']) ){
            $listRows = (int)$REQUEST['r'];
        }else{
            $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
        }

        $page = new \Think\Page($total, $listRows, $REQUEST);
        if($total>$listRows){
            $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p =$page->show();
        $this->assign('_page',  $p? $p: '');
        $this->assign('_total', $total);

        $workTime   = array(
            1 => '一周内',
            2 => '一月内',
            3 => '三月内',
            4 => '半年内',
        );
        $statusDesc = array(
            0 => '无状态',
            1 => '正常进行中',
            2 => '待审核'
        );
        $typeDesc = array(
            1=>'个人',
            2=>'设计院',
            3=>'公司'

        );

        $this->assign('typeDesc', $typeDesc);
        $this->assign('workTime', $workTime);
        $this->assign('statusDesc', $statusDesc);

        $res = $proModel->where($map)->order('update_time DESC')->limit($page->firstRow, $page->listRows)->select();
        if($res){
            $nowTime = time();
            foreach ($res as $k=>$v){
                $diedayTime = (isset($v['dieday']) && $v['dieday']) ? $v['dieday'] * 86400 : 604800;
                $endTime = intval($v['create_time']) + $diedayTime;//报价截止时间为一周
                $v['bidtime'] = ( $endTime > $nowTime ) ? ( $endTime - $nowTime ) : 0;

                $res[$k] = $v;
            }
        }
        $this->assign('_lists', $res);

        $this->display();
    }

    /**
     * 记录日志
     * @param $data
     */
    public function log($data)
    {
        if (is_array($data)) {
           $data =  json_encode($data);
        }
        \Think\Log::write($data,'DEBUG');
    }
}