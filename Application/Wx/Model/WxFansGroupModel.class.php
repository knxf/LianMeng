<?php
/**
 * 微信粉丝模型
 * 粉丝分组
 * wyqiang 16-09-08
 */
namespace Wx\Model;
use Think\Model;
class WxFansGroupModel extends Model {
	
	public function mpGroup($wxid) {
		return $this->where(array('wxid'=>$wxid))->order('`wx_group_id` ASC')->select();
	}
}
?>
