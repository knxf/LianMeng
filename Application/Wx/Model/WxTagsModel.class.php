<?php
/**
 * 微信标签模型
 * User: wyqiang
 * Date: 2016-5-30
 */
namespace Wx\Model;
use Think\Model;
class WxTagsModel extends Model {
	/**
	 * 获取标签id对应关系  wx_tag_id=>id
	 * @param int $wxid
	 * @return array
	 */
	public function getTagsWxidId($wxid){
		$list = $this->field('id,wx_tag_id')->where(array('wxid'=>$wxid))->select();
		$tagids = array();
		foreach($list as $item){
			$tagids[$item['wx_tag_id']] = $item['id'];
		}
		return $tagids;
	}

	/**
	 * 删除标签
	 * @param int $tagid
	 */
	public function delTagById(int $tagid){
		if($this->where(array('id'=>$tagid))->delete() === false){
			return false;
		}else{
			//删除关联表
			if(M('WxFansTag')->where(array('tag_id'=>$tagid))->delete() === false){
				return false;
			}else{
				return true;
			}
		}
	}
}
?>
