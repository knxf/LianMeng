<?php
/**
 * 微信标签关系模型
 * User: wayne
 * Date: 2016-5-29
 */
namespace Wx\Model;
use Think\Model;
use Think\Exception;

class WxFansTagModel extends Model {
	/**
	 * 获取标签信息
	 * @param $openid
	 * @param $wxid
	 * @return bool
	 */
	public function getTagInfo($openid,$wxid){
		if(empty($openid)){
			return false;
		}
		$map = array(
			'openid'=>$openid,
			't.wxid'=>$wxid,
			't.is_default'=>array('neq',1)
		);
		return $this->alias('a')->join('LEFT JOIN __WX_TAGS__ as t ON a.tag_id = t.id')
					->where($map)
					->select();
	}

	/**
	 * 批量为用户打标签
	 * @param array $openids
	 * @param array $tagids
	 * @param int $wxid
	 * @return bool
	 */
	public function batchAddFansTag(array $openids,array $tagids,int $wxid){
		$taglimit = C('TAG_LIMIT');
		$tagcount = count($tagids);
		$this->startTrans();
		try {
			if($tagcount>$taglimit){
				throw new Exception('每个粉丝最多打'.$taglimit.'个标签');
			}
			foreach($openids as $openid){
				//删除原有所有标签
				$delmap = array(
					'openid' =>$openid
				);
				if($this->where($delmap)->delete() === false){
					throw new Exception('删除原有标签错误');
				}else{
					$addinfo = array();
					foreach($tagids as $tagid){
						$addinfo[] = array(
							'openid' => $openid,
							'tag_id' => $tagid,
							'create_time' => time()
						);
					}
					if(!empty($addinfo)) {
						if ($this->addAll($addinfo) === false) {
							throw new Exception('发生数据库错误');
						}
					}
				}
			}
			//更新所有标签用户数量
			if($tagids){
				//查询所有标签
				$alltagids = M('WxTags')->field('id')->where(array('wxid'=>$wxid))->select();
				foreach($alltagids as $tagvo){
					//统计数量
					$count_tag = $this->where(array('tag_id'=>$tagvo['id']))->count();
					if(M('WxTags')->where(array('id'=>$tagvo['id']))->save(array('fans_count'=>$count_tag)) === false){
						throw new Exception('统计粉丝数量失败，请重新尝试！');
					}
				}
			}
		} catch (Exception $e) {
			$this->error = $e->getMessage();
			$this->rollback();
			return false;
		}
		$this->commit();
		return true;
	}

	/**
	 * 加入黑名单
	 * @param array $openids
	 * @param int $wxid
	 * @return bool
	 */
	public function addBlackTag(array $openids,int $wxid){
		$addinfo = array();
		$taginfo = M('WxTags')->where(array('wx_tag_id'=>1,'wxid'=>$wxid))->find();
		foreach($openids as $openid){
			$addinfo[] = array(
				'openid' => $openid,
				'tag_id' => $taginfo['id'],
				'create_time' => time()
			);
		}
		if(!empty($addinfo)) {
			if ($this->addAll($addinfo) === false) {
				return false;
			}else{
				//统计数量
				$count_tag = $this->where(array('tag_id'=>$taginfo['id']))->count();
				M('WxTags')->where(array('id'=>$taginfo['id']))->save(array('fans_count'=>$count_tag));
			}
		}
		return true;
	}

	/**
	 * 移出黑名单
	 * @param array $openids
	 * @param int $wxid
	 * @return bool
	 */
	public function removeBlackTag(array $openids,int $wxid){
		$black_taginfo = M('WxTags')->where(array('wxid'=>$wxid,'wx_tag_id'=>1))->find();
		$count_openid = count($openids);
		$delmap = array(
			'tag_id' => $black_taginfo['id'],
			'openid' => array('in',$openids)
		);
		if($this->where($delmap)->delete() === false){
			return false;
		}else{
			//更新数量
			M('WxTags')->where(array('id'=>$black_taginfo['id']))->setDec('fans_count',$count_openid);
			return true;
		}
	}
}
?>
